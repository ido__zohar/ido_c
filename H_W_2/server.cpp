#pragma comment (lib, "ws2_32.lib")
#include <iostream>
#include <winsock2.h>
#include <Windows.h>
#include <thread>

void my_server(int port);

int main()
{

	std::thread newthread(my_server, 2498);
	std::thread newthread2(my_server, 0204);

	newthread.join();
	newthread2.join();

	std::cout << "finished!" << std::endl;
	getchar();
	system("PAUSE");
	return 0;
}

void my_server(int port)
{
	WSADATA info;

	sockaddr_in servInfo;
	servInfo.sin_addr.s_addr = htonl(INADDR_ANY);
	servInfo.sin_port = htons(port);
	servInfo.sin_family = AF_INET;

	int err, listenSocket, cResut;
	SOCKET ClientSocket = INVALID_SOCKET;
	err = WSAStartup(MAKEWORD(2, 0), &info);
	if (err == 0)
	{
		listenSocket = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
		if (listenSocket != INVALID_SOCKET)
		{
			// Setup the TCP listening socket
			cResut = ::bind(listenSocket, (sockaddr*)&servInfo, sizeof(sockaddr));
			if (cResut == SOCKET_ERROR)
			{
				std::cout << "bind failed with error: " << WSAGetLastError() << std::endl;
			}
			else
			{
				cResut = listen(listenSocket, SOMAXCONN);
				if (cResut != 0)/*check if the listen is succed*/
				{
					std::cout << "Error listen. " << std::endl;
				}
				else
				{
					std::cout << "Listening on port " << port << std::endl;
					ClientSocket = accept(listenSocket, NULL, NULL);

					if (ClientSocket == INVALID_SOCKET)/*check if the accept is succed*/
					{
						std::cout << "accept failed with error: " << WSAGetLastError() << std::endl;
					}
					else
					{
						char recev[8];
						recv(listenSocket, recev, 8, 0);
						if (recev){
							send(ClientSocket, "Accepted!", 9, 0);
						}
						closesocket(ClientSocket);
					}
				}
			}
			closesocket(listenSocket);
		}
		else
		{
			std::cout << "Error creatinng socket" << std::endl;
		}
	}

	WSACleanup();
}
