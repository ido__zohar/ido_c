#pragma comment(lib, "Ws2_32.lib")

#include <iostream>
#include <winsock2.h>
#include <Windows.h>
#include <thread>

#define LEN 8
#define RECV 9

//Function Declaration:
void client(int port, int cNum);

void main()
{
	std::thread client1(client, 2498, 1);
	std::thread client2(client, 0204, 2);

	client1.join();
	client2.join();

	getchar();
}

void client(int port, int cNum)
{
	WSADATA newWSA;
	int res;
	res = WSAStartup(MAKEWORD(2, 0), &newWSA);
	if (res){
		std::cout << "WSAStartup failed. Error Code: " << res << std::endl;
	}
	else{
		int sockt = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
		if (sockt == INVALID_SOCKET){
			std::cout << "Socket Creating failed. Error Code: " << WSAGetLastError() << std::endl;
		}
		else{
			std::cout << "Socket Creating succeeded!" << std::endl;
			SOCKADDR_IN client;
			client.sin_family = AF_INET;
			client.sin_addr.s_addr = inet_addr("172.16.27.29");
			client.sin_port = htons(port);
			int res = connect(sockt, (struct sockaddr*) &client, sizeof(client));
			if (res == SOCKET_ERROR){
				std::cout << "Socket Connect failed. Error Code: " << WSAGetLastError() << std::endl;
				res = closesocket(sockt);
				if (sockt == SOCKET_ERROR){
					std::cout << "Socket Close failed. Error Code: " << WSAGetLastError() << std::endl;
				}
				WSACleanup();
			}
			else{
				char recvBuf[RECV];
				res = recv(sockt, recvBuf, RECV, 0);
				if (res == SOCKET_ERROR){
					std::cout << "Receive failed. Error Code: " << WSAGetLastError() << std::endl;
				}
				else{
					std::cout << "Receive succeeded!" << std::endl;
					std::cout << "Data Recieved: " << recvBuf << std::endl;
				}
			}
		}
		res = closesocket(sockt);
		if (sockt == SOCKET_ERROR){
			std::cout << "Socket Close failed. Error Code: " << WSAGetLastError() << std::endl;
		}
		WSACleanup();
	}

	std::cout << "Finished With Client Number " << cNum << std::endl;
}