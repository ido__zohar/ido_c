#include "Player.h"

int Player::getCardsNum(){ return this->myCards.size(); }
void Player::setPlace(int myPlace){ this->place = myPlace; }
int Player::getPlace(){ return this->place; }
char * Player::getIp(){ return this->ip; }
void Player::setDetails(char * my_ip, string my_name, string my_last_name, string my_email)
{
	this->ip = my_ip;
	this->name = my_name;
	this->last_name = my_last_name;
	this->email = my_email;
}

Player * Player::returnPlayer()
{
	return this;
}

Player::Player(){}
Player::~Player(){}