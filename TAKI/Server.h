#pragma once

#define _CRT_SECURE_NO_WARNINGS
#define _WINSOCK_DEPRECATED_NO_WARNINGS
#define DBNTWIN32

#include <thread>
#include <WinSock2.h>
#include <windows.h>
#include <vector>
#include <iostream>
#include <string>
#include <typeinfo>
#include <conio.h>
#include <math.h>
#include <exception>
#include "sqlite3.h"

using namespace std;

/*PLAYER NUMBERS*/

#define P_NUM 4
#define STRING_MAX_LENGTH 2048
#define LENGTH 100
#define MESSAGE_SIZE 1024

/*ALL THE COLORS DISPLAYS ON BACK TEXTURE*/

#define BLUE 11
#define YELLOW 14
#define GREEN 10
#define RED 12
#define WHITE 12

/*COMMANDS SERVER CLIENT*/

// client
#define EN_REGISTER "1" // ask to register the player
#define EN_LOGIN "2" // ask to log in the user
#define EN_LOGOUT "3" // ask to log out the user
#define RM_ROOM_LIST "10" // ask to show room list
#define RM_CREATE_GAME "11" // ask to create game (room)
#define RM_JOIN_GAME "12" // ask to join the selected room
#define RM_START_GAME "13" // the player is ready! start the game
#define RM_LEAVE_GAME "14" // ask to leave the game
#define RM_CLOSE_GAME "15" // ask to close the game (all the players get out)
#define GM_PLAY "20" // ask to play turn
#define GM_DRAW "21" // ask to draw cards
#define CH_SEND "30" // ask to send messege
#define EN_CHANGE_COLOR_IS "31" // client send to server + set new color
#define KING_PUT_MORE_ONE "32" // the player can put another card (king card)
#define CREATE_ROOM "33" // request to create room
#define ASK_LOG_IN "34" // request to log in your user

// server
#define PGM_SCC_LOGIN "100" // succefuly log in
#define PGM_SCC_REGISTER "101" // succefully registeration
#define PGM_SCC_GAME_CREATED "102" // room succesfully created
#define PGM_SCC_GAME_JOIN "103" // join the game/ room succefully
#define PGM_SCC_GAME_CLOSE "104" // succefully exit game
#define PGM_SCC_ROOM_CREATION "105" // succesfully create roon, messege to the player to log in
#define PGM_ERR_LOG_IN "106" // password or email not correct

// server
#define PGM_CTR_NEW_USER "110" // create new user
#define PGM_CTR_REMOVE_USER "111" // delete user
#define PGM_CTR_GAME_STARTED "112" // game was started
#define PGM_CTR_ROOM_CLOSED "113" // room was closed
#define PGM_CTR_ROOM_LIST "114" // retrun room list
#define PGM_ERR_LOGIN "120" // error log in
#define PGM_ERR_ROOM_FULL "121" // room is full
#define PGM_ERR_EMAIL_TAKEN "122" // the name (email) already exist
#define PGM_CTR_CHANGE_COLOR "123" // send messege to the client to change color --> the client choose color
#define ROOM_NAME_ALREADY_EXIST "124" // room name already exist
#define PGM_ERR_TOO_FEW_USERS "125" // to few users to start the game
#define PGM_ERR_INFO_TOO_LONG "126" // the info is over the maximum length
#define PGM_ERR_WRONG_PASSWORD "127" // password is too short (6 letters or more)
#define PGM_ERR_ROOM_NOT_FOUND "128" // room was not found
#define ERR_JOIN_ROOM_GAME_STARTED "129" // cannot join the room because game already started!


// fatal errors
#define PGM_MER_MESSAGE "130" // messege dosent fit to the protocol
#define PGM_MER_ACCESS "131" // messege dosent fit or no authorization
#define PGM_ERR_REGISTERR_INFO "150" // error registration


// game messeges
#define GAM_SCC_TURN "200" // succes turn
#define GAM_SCC_DRAW "201" // succes draw
#define GAM_CTR_TURN_COMPLETE "210" // turn complete
#define GAM_CTR_DRAW_CARDS "211" // one of the players draw cards
#define GAM_CTR_GAME_ENDED "212" // game ended
#define PLAYER_LEFT_THE_ROOM "213"

// illigal movemeant
#define GAM_ERR_ILLEGAL_CARD "220" // illigal card!
#define GAM_ERR_ILLEGAL_ORDER "221" // illigal order!
#define GAM_ERR_LAST_CARD "222" // last card was uncorrect
#define GAM_ERROR_WRONG_DRAW "223" // draw dosent succeeded
#define EXIT_GAME "555"

// chat
#define CHA_SCC "300" // succeeded sending chat messege
#define CHA_ERR "310" // problem sending chat messege

class Server{

public:

	bool Listen();
	bool stopListen();

	thread * startAccept(); // tread (working with the main), gets requests from the clients
	void recvMessege(int socket); // function who call by the server (me), recv the messege and sending her to destruction (also by tread)... 

	int getPort(); // return the port of the currect user
	void sendToDirectClient(char * ip, char * messege); // send to specific client by IP [example for using: CHAT]

	Server(int port); // constructor
	Server(); // default Constructor
	~Server(); // destructor


private:

	int _port;
	int _socket;
	WSADATA _info;
	sockaddr_in _addi;
};