#pragma once

#include "Server.h"

/* FUNCTIONS THAT ARE RESPONSIBLE FOR DATA BASE */

int callback(void* notUsed, int argc, char** argv, char** azCol); // Auxiliary function for the exec_sqlite3 function

bool createDbAndTable(); // create dataBase & table --> 0 = sucseeded, 1 = faild 

int createPlayer(char email[LENGTH], char name[LENGTH], char last_name[LENGTH], char password[LENGTH]); // create Player --> 0 = faild, every other number = ID 

bool logIn(char email[LENGTH], char password[LENGTH]); // check if the user exists in the data-base and log's in (false = log in succeeded! 1 = faild)