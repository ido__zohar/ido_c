#pragma once

#include "Server.h"
#include "Messege.h"

#pragma comment(lib,"ws2_32.lib")

Server::Server(){}

Server::Server(int port)
{
	this->_port = port;

	if (WSAStartup(MAKEWORD(2, 0), &_info))
	{
		throw "WSAStartup failed";
	}

	_socket = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
	if (_socket == INVALID_SOCKET)
	{
		WSACleanup();
		throw "socket failed";
	}

	_addi.sin_family = AF_INET;
	_addi.sin_addr.S_un.S_addr = inet_addr("127.0.0.1");
	_addi.sin_port = htons(_port);
	
	
	if (::bind(_socket, (struct sockaddr *)&_addi, sizeof(struct sockaddr_in)) == SOCKET_ERROR)
	{
		//closesocket(_socket);
		WSACleanup();
		throw "bind failed";
	}



	std::cout << "Bind succeeded" << std::endl;

}

Server::~Server()
{
	closesocket(_socket);
	WSACleanup();
}

//////////////////////////////////////////////////////////////////////////////

bool Server::Listen()
{
	if (listen(_socket, SOMAXCONN) == SOCKET_ERROR)
	{
		std::cout << "Error in listening" << std::endl;
		return false;
	}

	std::cout << "listening to port " << _port << " ..." << std::endl;
	return true;
}

thread * Server::startAccept()
{
	int x = sizeof(struct sockaddr_in);
	sockaddr  myclient;
	int clientsocket = accept(_socket, (sockaddr *)&myclient, &x);

	thread* t = new thread(&Server::recvMessege, this, clientsocket);
	return t;
}


void Server::recvMessege(int socket)
{
	cout << "new client..." << endl;
	char myMsg[STRING_MAX_LENGTH] = "";
	struct sockaddr_in addr;
	int addr_size = sizeof(struct sockaddr_in);
	int res = getpeername(socket, (struct sockaddr *)&addr, &addr_size);


	cout << "Handle client: socket=" << socket << ", ip=" << inet_ntoa(addr.sin_addr) << endl;
	int r = ::recv(socket, myMsg, STRING_MAX_LENGTH, 0);

	cout << "messege = " << myMsg << endl;
	createMessege(myMsg, socket, inet_ntoa(addr.sin_addr));
	// create new thread that will handle the messege and send a messege to the client

    //closesocket(socket);
}

int Server::getPort(){ return this->_port; }

//// work on this
void Server::sendToDirectClient(char * ip, char * messege)
{

}