#pragma once

#include "DataBase.h"
#include "Messege.h"

/* GLOBAL VALUES */
sqlite3 * db = NULL;
char * zErrMsg = 0;
int rc;
int checkExEmail = 0;
char passwordCheck[LENGTH];

int callback(void* notUsed, int argc, char** argv, char** azCol)
{
	int i;

	for (i = 0; i < argc; i++)
	{
		cout << azCol[i] << " = " << argv[i] << endl;
		if ((strcmp(azCol[i], "count(*)")) == 0){ checkExEmail = atoi(argv[i]); }
		if (strcmp(azCol[i], "Password") == 0){ strcpy(passwordCheck, argv[i]); }
	}

	return 0;
}

bool createDbAndTable()
{
	rc = sqlite3_open("mydb.db", &db);
	if (rc)
	{
		cout << "Can't open data base" << endl;
		sqlite3_close;
		return true;
	}
	// create table
	rc = sqlite3_exec(db, "CREATE TABLE \"players\"(Password string, Name string, Last_Name string, Email string, Points int);", callback, 0, &zErrMsg);
	//sqlite3_close;
	return false;
}

int createPlayer(char email[LENGTH], char name[LENGTH], char last_name[LENGTH], char password[LENGTH])
{
	int i = 0;
	char checkExistingEmail[MESSAGE_SIZE];
	char createUser[MESSAGE_SIZE];

	checkExEmail = 0;

// commands for email and name checks
	strcpy(checkExistingEmail, "select count(*) from players where Email = '"); // check
	strcat(checkExistingEmail, email);
	strcat(checkExistingEmail, "'");

	// 0 = found, 1 = faild -->>> CHECK THE NUMBERS AGAIN!!
	
	if (strlen(password) < 4 || strlen(password) > 16)
	{
		return 3; // password is too long
	}

	rc = sqlite3_exec(db, checkExistingEmail, callback, 0, &zErrMsg);
//	cout << endl << checkExEmail << endl << endl;
	if (checkExEmail == SQLITE_OK) // email dosent exist
	{
		// we should sign (add) the user to the data base and send him a ID
		strcpy(createUser, "INSERT INTO players (Password,Name, Last_Name, Email) VALUES('");
		strcat(createUser, password);
		strcat(createUser, "','");
		strcat(createUser, name);
		strcat(createUser, "','");
		strcat(createUser, last_name);
		strcat(createUser, "','");
		strcat(createUser, email);
		strcat(createUser, "');");

		rc = sqlite3_exec(db, createUser, callback, 0, &zErrMsg);
		if (rc != SQLITE_OK) // faild creating user
		{  fprintf(stderr, "SQL error: %s\n", zErrMsg); sqlite3_free(zErrMsg);	}
		else
		{
			return false; // succeeded regestration
		}
	}
	else
	{ 
		cout << "user already exists." << endl;
		return true; // user alreay exists
	}
	return 2; // system error
}

bool logIn(char email[LENGTH], char password[LENGTH])
{
	char checkExistingEmail[MESSAGE_SIZE];
	char checkExistingPassword[MESSAGE_SIZE];

	checkExEmail = 0;

	// commands for email and password check
	strcpy(checkExistingEmail, "select count(*) from players where Email = '"); // check
	strcat(checkExistingEmail, email);
	strcat(checkExistingEmail, "'");


	rc = sqlite3_exec(db, checkExistingEmail, callback, 0, &zErrMsg);
	if (rc != SQLITE_OK) { fprintf(stderr, "SQL error: %s\n", zErrMsg); sqlite3_free(zErrMsg); }

	if (checkExEmail != SQLITE_OK) // email EXIST --> check for password (if exist too)
	{
		strcpy(checkExistingPassword, "SELECT Password FROM 'players' WHERE Email = '"); // return the password
		strcat(checkExistingPassword, email);
		strcat(checkExistingPassword, "'");

		rc = sqlite3_exec(db, checkExistingPassword, callback, 0, &zErrMsg);
		if (rc != SQLITE_OK) { fprintf(stderr, "SQL error: %s\n", zErrMsg); sqlite3_free(zErrMsg); }

		if ((strcmp(passwordCheck, password)) == 0)
		{
			// user exists you able to log in!
			cout << "succeeded login!" << endl;
			return false;
		}
		else{
			cout << "faild login..." << endl;
			return true;
			// user dosent exist.. faild.
		}
	}
	else{
		cout << "faild login..." << endl;
		return true; 
	// user dosent exist.. faild.
	}
	return 0;
}