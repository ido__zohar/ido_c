#include "Room.h"

void Room::printGame(){} 

void Room::randomCards(){} 

void Room::isItOk(Card check, int socket)
{
	char * recvMesssege = "";
	char changeClr[LENGTH] = "";
	char drawCards[LENGTH] = ""; // [0] = order, space, [1] = number of cards
	int i = 0, j = 0;
	bool spaceCounter = false;

// NULL CARDS SPECIAL COLOR AND NUMBER
	if ((strcmp(last_card.getName().c_str(), "+2") == false))
	{
		if (strcmp(check.getName().c_str(), "+2") != false)
		{
			// drawCards creation
		}
	}

// SPECIAL CARDS CHECK
	if ((check.getName().compare("+2") == false) && (check.getColor() == last_card.getColor())) // +2 check
	{
		if (strcmp(last_card.getName().c_str(), "+2") == false) /*FINISH*/
		{
			this->plusTwoCounter += 2; // raise the counter by two
		}

		last_card.setColor(check.getColor());
		last_card.setName(check.getName());

		send(socket, GAM_SCC_TURN, strlen(GAM_SCC_TURN), 0);
		if (this->getDirection() == false) { this->playerTurn += true; } // right
		if (this->getDirection() == true) { this->playerTurn -= true; } // left
	}

	if ((strcmp(last_card.getName().c_str(), "KING")) == false) /*FINISH*/
	{
		last_card.setColor(check.getColor());
		last_card.setName(check.getName());
		send(socket, KING_PUT_MORE_ONE, strlen(KING_PUT_MORE_ONE), 0);
	}

	if ((check.getName().compare("TAKI") == false) && (check.getColor() == last_card.getColor())) // taki check // no raising turn
	{
		last_card.setColor(check.getColor());
		last_card.setName(check.getName());
		this->openTaki = true;
		/*for (i = 0; i < this->players[this->playerTurn]->getCardsNum; i++)
		{
			/////// CHECK IF THE PLAYER HAS MORE CARDS IN THE COLOR
		}*/
	}

	if ((check.getName().compare("SUPER_TAKI") == false)) // super taki check // no raising turn
	{
		last_card.setName(check.getName());
		last_card.setColor(WHITE);
		this->openTaki = true;

		// send messege to the player to choose color, and then is just like normal "TAKI" //
	}

	if ((check.getName().compare("STOP") == false) && (check.getColor() == last_card.getColor()))
	{
		send(socket, GAM_SCC_TURN, strlen(GAM_SCC_TURN), 0);
		if (this->getDirection() == false) { this->playerTurn += 2; } // right
		if (this->getDirection() == true) { this->playerTurn -= 2; } // left
	}

	if ((check.getName().compare("DIRECTION") == false) && (check.getColor() == last_card.getColor()))
	{
		this->setDirection();
	}

	if ((check.getName().compare("CHANGE_COLOR") == false) && (check.getColor() == last_card.getColor()))
	{
		last_card.setName(check.getName());
		send(socket, PGM_CTR_CHANGE_COLOR, strlen(PGM_CTR_CHANGE_COLOR), 0); // send messege to change color
		recv(socket, recvMesssege, strlen(recvMesssege), 0); // messege with the color
		
		for (i = 0; i < strlen(recvMesssege); i++)
		{
			if (spaceCounter == true)
			{
				changeClr[j] = recvMesssege[i];
				j++;
			}
			if (recvMesssege[i] == ' '){ spaceCounter = true; }
		}
		this->last_card.setColor(atoi(changeClr));
	}


// NORMAL CARDS 1 - 9 CHECK

	if (check.getColor() == this->last_card.getColor()) // equal by color
	{
		last_card.setColor(check.getColor());
		last_card.setName(check.getName());

		send(socket, GAM_SCC_TURN, strlen(GAM_SCC_TURN), 0);
		if (this->getDirection() == false) { this->playerTurn += true; } // right
		if (this->getDirection() == true) { this->playerTurn -= true; } // left
	}

	if (check.getName() == this->last_card.getName()) // equal by number
	{
		last_card.setColor(check.getColor());
		last_card.setName(check.getName());

		send(socket, GAM_SCC_TURN, strlen(GAM_SCC_TURN), 0);
		if (this->getDirection() == false) { this->playerTurn += true; } // right
		if (this->getDirection() == true) { this->playerTurn -= true; } // left
	}
}

///////////////////////////////////////////////////////////////////
Card Room::getLastCard()
{ return this->last_card; } 

void Room::setTurn()
{
	int i, players_connect = 0;
	for (i = 0; i < P_NUM; i++)
	{
		if (this->players[i] != NULL){ players_connect++; }
	}
	for (i = 0; i < players_connect; i++)
	{
		if (this->direction == false) // right 1 -> 2 -> 3 -> 4 -> 1...
		{
			if (this->playerTurn == players_connect){ this->playerTurn = 1; }
			else { this->playerTurn++; }
		}
		else{ 
			// left 4 -> 3 -> 2 -> 1 -> 4...
			if (this->playerTurn == 1){ this->playerTurn = players_connect; }
			else{ this->playerTurn--; }
		}
	}
} 

int Room::getTurn()
{ return this->playerTurn; }

bool Room::getDirection()
{ return this->direction; }

void Room::setDirection()
{
	if (this->direction == true){ this->direction = false; }
	if (this->direction == false){ this->direction = true; }
} 

void Room::setPlusTwo(int num) 
{
	if (num == 0){ this->plusTwoCounter = 0; }
	if (num == 1){ this->plusTwoCounter += 2; }
} 

int Room::getPlusTwo()
{ return this->plusTwoCounter; } 

bool Room::getOpenTaki()
{ return this->openTaki; } 

void Room::setOpenTaki(bool turnOfOn)
{ this->openTaki = turnOfOn; } 

void Room::setRoomName(string name)
{ this->roomName = name; }

string Room::getRoomName()
{ return this->roomName; }

void Room::addPlayer(Player * myNewPlayer)
{
	int i;
	for (i = 0; i < P_NUM; i++)
	{
		if (this->players[i] == NULL)
		{
			this->players[i] = myNewPlayer;
		}
	}
}

void Room::setPlaying()
{
	if (this->playing == true){ this->playing = false; }
	if (this->playing == false){ this->playing = true; }
}

bool Room::getPlaying()
{
	return this->playing;
}

Room::Room(){} 

Room::~Room(){}