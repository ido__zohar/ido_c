#pragma once

#include "Server.h"

/*  NAMES OF CARDS IDENTIFICATION - INITIAL */

// SHAPE OF CARD EXAMPLE --> [ +2 ], OPPONENT CARD (OPPOSITE TO THE SIDE) --> { ? } //

// KING = "K" + COLOR = WHITE //
// +2 = "+2" + COLOR //
// SUPER TAKI = "SUPER_TAKI" + IDENTIFICATION COLOR = WHITE, REAL COLOR = ALL COLORS (4) //
// TAKI = "TAKI" + COLOR = BLUE, YELLOW, GREEN, RED //
// CHANGE COLOR = "CHANGE_COLOR" + IDENTIFICATION COLOR = WHITE, REAL COLOR = ALL COLORS (4) //
// STOP = "STOP" + COLOR //
// CHANGE DIRECTION = "^" + COLOR //
// PLUS = "+" + COLOR //
// 1 - 7 = "N" + COLOR //

/*	IF MAIN COLOR = WHITE --> EVERY COLOR IS EXEPTED */
/* IF NAME == "" --> EVERY NAME IS EXEPTED */

class Card{
private:
	int color; // white = 12 (SPECIAL CARD), green = 10 / 2, blue = 11, red = 12, yellow = 14
	string name; // name of card: 7, +, ss (posivle options)....

public:
	void setColor(int myColor); // sets the color of the card
	int getColor(); // gets the color of the card
	void setName(string myName); // sets the name of the card
	string getName(); // gets the name of the card

	Card(); // default constructor
	~Card(); //default destructor
};