#include "Messege.h"
#include "DataBase.h"

// Global = Rooms (vector)
vector<Room> myRooms; // all the "working rooms"
vector <Player> allThePlayers; // all the players who connect to the server right now

// MESSEGE CLASS = [0] = number of messege , [1 - Contents message] = class of currect messege 

void createMessege(char messege[STRING_MAX_LENGTH], int socket, char * senderIp)
{
	int i = 0, spaceCounter = 0, j = 0, k = 0, reply, RoomNum, c = 0, n = 0, numOfwords = 0, exit = 0, connectP_counter = 0, currectP, currectRoom;;
	int s; // socket
	char * currectMessege = ""; // the destruct messege like " sdfsdf  sdfsdf "
	bool res;

	string spaceMessege[LENGTH]; // [j][k] counter --> arr of part of the messege
	for (i = 0;i < strlen(messege);i++)
	{
		if (messege[i] == ' ') { spaceCounter++; }
	}


	char * pch;
	pch = strtok(messege, " ");
	spaceMessege[0] = pch;
	i = 1;

	// destruct the messege
	for (j = 0;j < spaceCounter;j++)
	{
		pch = strtok(NULL, " ");
		spaceMessege[i] = pch;
		i++;
	}

	
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// REQUEST TO CREATE NEW USER

	if ((spaceMessege[0].compare(EN_REGISTER)) == 0) // [1] = email, [2] = name, [3] = last name, [4] = password
	{
		char email[LENGTH] = "";
		char name[LENGTH] = "";
		char password[LENGTH] = "";
		char last_name[LENGTH] = "";

		strcpy(email, spaceMessege[1].c_str()); // email (1)
		strcpy(name, spaceMessege[2].c_str()); // name (2)
		strcpy(last_name, spaceMessege[3].c_str()); // last name (3)
		strcpy(password, spaceMessege[4].c_str()); // password (4)

		reply = createPlayer(email, name, last_name, password); // creating NEW USER // reply --> || 0 = succeeded, 1 = already exists, 2 = system error, 3 = password d'ont match
		if (reply == 0){
			send(socket, PGM_SCC_REGISTER, strlen(PGM_SCC_REGISTER), 0);
			Player * newP = new Player(); // adds the player to the list of the players
			newP->setDetails(senderIp, spaceMessege[2], spaceMessege[3], spaceMessege[1]);
			allThePlayers.push_back(*newP);
		}
		if (reply == 1){
			send(socket, PGM_ERR_EMAIL_TAKEN, strlen(PGM_ERR_EMAIL_TAKEN), 0);
		}
		if (reply == 2){
			send(socket, PGM_ERR_REGISTERR_INFO, strlen(PGM_ERR_REGISTERR_INFO), 0);
		}
		if (reply == 3){
			send(socket, PGM_ERR_WRONG_PASSWORD, strlen(PGM_ERR_WRONG_PASSWORD), 0);
		}
	}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// REQUEST TO CHAT WITH YOUR ROOMMATES

	if ((spaceMessege[0].compare(CH_SEND)) == 0) // [0] = request, [1 - end] = "Messege to send" (idedification using IP)
	{
		strcpy(currectMessege, spaceMessege[1].c_str());
		for (i = 2; i < numOfwords; i++)
		{
			strcat(currectMessege, " ");
			strcat(currectMessege, spaceMessege[i].c_str());
		}

		RoomNum = myRooms.size();
		for (c = 0; c < RoomNum; c++)
		{
			for (n = 0; n < P_NUM; n++)
			{
				if (myRooms.at(c).players[n] != NULL)
				{
					if (0 == (strcmp(myRooms.at(c).players[n]->getIp(), senderIp)))
					{
						for (i = 0; i < P_NUM; i++)
						{
							if (myRooms.at(c).players[n] != NULL)
							{
								// send the messege to the players --> specificly using their IP
								sockaddr_in ClientService;
								s = ::socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
								ClientService.sin_family = AF_INET;
								ClientService.sin_addr.s_addr = inet_addr(myRooms.at(c).players[n]->getIp());

								// currect messege is the messege [0] = order, [1] = "words to print"
								send(s, currectMessege, strlen(currectMessege), 0); // send directly
								closesocket(s);
							}
							if (i == P_NUM - 1){ n = P_NUM; c = P_NUM; }
						}
					}
				}
			}
		}
	}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// REQUEST TO CREATE ROOM

	if ((spaceMessege[0].compare(CREATE_ROOM)) == 0) // [0] = request, [1] = "room name"
	{
		for (i = 0; i < myRooms.size(); i++)
		{
			if ((strcmp(myRooms[i].getRoomName().c_str(), spaceMessege[1].c_str())) == 0) // room name already EXIST! faild 
			{
				send(socket, ROOM_NAME_ALREADY_EXIST, strlen(ROOM_NAME_ALREADY_EXIST), 0);
				exit = true;
				break;
			}
		}
		if (exit == 0)
		{
			Room * createRoom = new Room();
			createRoom->setRoomName(spaceMessege[1]);

			for (i = 0; i < allThePlayers.size(); i++)
			{
				if ((strcmp(allThePlayers[i].getIp(), senderIp)) == 0)
				{
					createRoom->addPlayer(allThePlayers[i].returnPlayer());
					send(socket, PGM_SCC_ROOM_CREATION, strlen(PGM_SCC_ROOM_CREATION), 0);
				}
			}
		}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////
// REQUEST TO START GAME

		connectP_counter = 0;
		for (i = 0; i < myRooms.size(); i++) // [0] = requesr, idefication using ip 
		{
			for (j = 0; j < P_NUM; j++)
			{
				if (myRooms[i].players[j] != NULL)
				{
					if (strcmp(myRooms[i].players[j]->getIp(), senderIp))
					{
						// check if there is at least 2 players and send them the currect messeges
						for (k = 0; k < P_NUM; k++)
						{
							if (myRooms[i].players[k] != NULL)
							{
								connectP_counter++; 
							}
							if (connectP_counter >= 2)
							{
								myRooms[i].setPlaying();
								for (c = 0; c < P_NUM; c++)
								{
									if (myRooms[i].players[c] != NULL)
									{
										// send messege that the game is starts
										sockaddr_in ClientService;
										s = ::socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
										ClientService.sin_family = AF_INET;
										ClientService.sin_addr.s_addr = inet_addr(myRooms.at(i).players[c]->getIp());

										send(s, RM_START_GAME, strlen(RM_START_GAME), 0); // GAME STARTS!
										closesocket(s);
										// need to give cards for every player
									}
									if (c == P_NUM - 1)
									{
										sockaddr_in ClientService;
										s = ::socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
										ClientService.sin_family = AF_INET;
										ClientService.sin_addr.s_addr = inet_addr(myRooms.at(i).players[1]->getIp());

										send(s, GM_PLAY, strlen(GM_PLAY), 0); // FIRST PLAYER STARTS PLAY
										break;
									}
								}
							}
							else
							{
								send(s, PGM_ERR_TOO_FEW_USERS, strlen(PGM_ERR_TOO_FEW_USERS), 0); // there is only one player
							}
						}
					}
				}
			}
		}
	}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// REQUEST TO JOIN GAME

	connectP_counter = 0;

	if (spaceMessege[0].compare(RM_JOIN_GAME) == 0) // [0] = order, [1] = "room name"
	{ 
		for (i = 0; i < myRooms.size(); i++)
		{
			for (j = 0; j < P_NUM; j++)
			{
				if (myRooms[i].players[j] != NULL)
				{
					if ((strcmp(myRooms[i].players[j]->getIp(), senderIp)) == 0)
					{
						currectP = j;
						if (myRooms[i].getPlaying() == true)
						{
							send(socket, ERR_JOIN_ROOM_GAME_STARTED, strlen(ERR_JOIN_ROOM_GAME_STARTED), 0);
							break;
						}
						for (k = 0; k < P_NUM; k++)
						{
							if (myRooms[i].players[j] != NULL)
							{
								connectP_counter++;
							}
						}
						if (connectP_counter > P_NUM - 1)
						{
							// room is full
							send(socket, PGM_ERR_ROOM_FULL, strlen(PGM_ERR_ROOM_FULL), 0);
						}
						else{
							// add player to the room
							myRooms[i].addPlayer(myRooms[i].players[currectP]->returnPlayer());
							send(socket, PGM_SCC_GAME_JOIN, strlen(PGM_SCC_GAME_JOIN), 0);
						}
					}
				}
			}
		}
	}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// REQUEST TO LOG IN TO YOUR USER

	char * smEmail = new char[LENGTH];
	char * smPass = new char[LENGTH];

	strcpy(smEmail, spaceMessege[1].c_str());
	strcpy(smPass, spaceMessege[2].c_str());

	if (spaceMessege[0].compare(EN_LOGIN) == 0) // [0] = order, [1] = email, [2] = password
	{
		res = logIn(smEmail, smPass);
		if (res == false)
		{
			send(socket, PGM_SCC_LOGIN, strlen(PGM_SCC_LOGIN), 0); // succeeded login
		}
		else
		{
			send(socket, PGM_ERR_LOG_IN, strlen(PGM_ERR_LOG_IN), 0); // faild login
		}
	}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// REQUEST TO LEAVE ROOM

	connectP_counter = 0;
	
	if (spaceMessege[0].compare(RM_LEAVE_GAME) == 0)
	{
		for (i = 0; i < myRooms.size(); i++)
		{
			for (j = 0;j < P_NUM;j++)
			{
				if (myRooms[i].players[j] != NULL)
				{
					if ((strcmp(myRooms[i].players[j]->getIp(), senderIp)) == 0)
					{
						myRooms[i].players[j] = NULL;
						currectRoom = i;
					}
					else {
						sockaddr_in ClientService;
						s = ::socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
						ClientService.sin_family = AF_INET;
						ClientService.sin_addr.s_addr = inet_addr(myRooms.at(i).players[j]->getIp());

						send(s, PLAYER_LEFT_THE_ROOM, strlen(PLAYER_LEFT_THE_ROOM), 0); 
						closesocket(s);
						connectP_counter++;
					}
				}
			}
		}
		if (connectP_counter == 0)
		{
//			myRooms.erase.at(currectRoom);
		}
	}
	

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// REQUEST TO EXIT GAME

	if ((strcmp(spaceMessege[0].c_str(), EXIT_GAME)) == 0)
	{
		for (i = 0; i < myRooms.size(); i++)
		{
			for (j = 0;j < P_NUM;j++)
			{
				if (myRooms[i].players[j] != NULL)
				{
					if ((strcmp(myRooms[i].players[j]->getIp(), senderIp)) == 0)
					{
						myRooms[i].players[j] = NULL;
						currectRoom = i;
					}
					else {
						sockaddr_in ClientService;
						s = ::socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
						ClientService.sin_family = AF_INET;
						ClientService.sin_addr.s_addr = inet_addr(myRooms.at(i).players[j]->getIp());

						send(s, PLAYER_LEFT_THE_ROOM, strlen(PLAYER_LEFT_THE_ROOM), 0);
						closesocket(s);
						connectP_counter++;
					}
				}
			}
		}
		if (connectP_counter == 0)
		{
		//	myRooms.erase.at(currectRoom);
		}
		for (i = 0; i < allThePlayers.size(); i++)
		{
			if ((strcmp(allThePlayers[i].getIp(), senderIp)) == 0)
			{
//				allThePlayers.erase.at(i);
			}
		}
	}

	//send(socket, GAM_ERR_ILLEGAL_ORDER, strlen(GAM_ERR_ILLEGAL_ORDER), 0);
}

