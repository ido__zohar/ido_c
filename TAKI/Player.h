#pragma once

#include "Card.h"

class Player{

private:

	string email; // name of the player
	string last_name; // last name of the player
	int num_of_cards; // number of cards in player's hand
	int victory_place; // 0 - 4 (see above)
	int place; // 0 = playing, 1 = finish
	vector<Card> myCards; // the cards in the player's hand
	string name; // players name
	char * ip; // players connections ip

public:
	
	int getCardsNum(); // return the number of cards in player's hand
	void setPlace(int myPlace); // remove the player in case of victory, 0 = playing... || 1, 2, 3, 4 = place (winning)
	int getPlace(); // what place is the player?
	char * getIp();
	void setDetails(char * my_ip, string my_name, string my_last_name, string my_email);
	Player * returnPlayer();

	Player(); // default constructor
	~Player(); // default destructor
};

