#pragma once

#include "Player.h"

class Room : public Player{

public:
	void printGame(); // print the game CMDSTYLE
	void randomCards(); // sets the coupe when the game starts, and Department cards
	void isItOk(Card check, int socket); // all the check of the rules inside.
	Card getLastCard(); // gets the last card

	void setTurn(); // 1, 2, 3, 4... repiding or change direction (other function)
	int getTurn(); // get the turn 1 - 4
	bool getDirection(); // get the direction --> right or left
	void setDirection(); //  0 = right, 1 = left
	void setPlusTwo(int num); // sets the counter of plus two card (+2 or 0) // false (0) = 0 -> counter, true (1) = +2 -> counter
	int getPlusTwo(); // gets the counter of plus two card
	bool getOpenTaki(); // gets the taki mode
	void setOpenTaki(bool turnOfOn); // sets the taki mode
	void setRoomName(string name); // sets the room name
	string getRoomName(); // gets the room name
	Player * players[P_NUM]; // arr of pointers to the player
	void addPlayer(Player * myNewPlayer); // add new player to the room
	void setPlaying(); // if they are not playing the function starts the game and the opposite
	bool getPlaying(); // return the mode of the game --> true = game started, false = waiting...
	Room(); // default constructor
	~Room(); // default destructor

private:
	Card last_card; // last card who played
	bool direction; // game direction: 1 = left, 0 = right
	vector<Card*> coupe; // the coupe of the game
	int playerTurn = true; // player 1 -4, who plays now???
	int plusTwoCounter; // the counter of plus 2 card raise every time by 2
	bool openTaki; // open = true, close = false
	string roomName; // the room name, chosen by the client / player
	bool playing = false; // do the players play already in this room? 
};